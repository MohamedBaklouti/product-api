<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        //create 10 products
        factory(App\Product::class, 20)->create();
        factory(App\User::class, 20)->create();
    }
}
